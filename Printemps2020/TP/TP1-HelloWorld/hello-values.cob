        IDENTIFICATION DIVISION.
        PROGRAM-ID. HELLO.
       
        DATA DIVISION.
          WORKING-STORAGE SECTION.
          01 NUM1 PIC S9(3)V9(2).
          01 NUM2 PIC P99999.
          01 NUM3 PIC S9(3)V9(2) VALUE -123.45.
          01 USERNAME PIC A(6) VALUE 'ABCDEF'.
          01 USERID PIC X(5) VALUE 'A121$'.
       
        PROCEDURE DIVISION.
           DISPLAY "NUM1 : "NUM1.
           DISPLAY "NUM2 : "NUM2.
           DISPLAY "NUM3 : "NUM3.
           DISPLAY "NAME : "USERNAME.
           DISPLAY "ID : "USERID.
           STOP RUN.