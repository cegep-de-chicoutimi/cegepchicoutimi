//SY20ZR24 JOB 1,DISKMAP,MSGLEVEL=(1,1),MSGCLASS=A
//*MAIN LINES=100
//*********************************************************************
//*                                                                 ***
//*    Job:      DISKMAP                                            ***
//*    Product:  MVT 21.8 with ASP V3.2.                            ***
//*    Purpose:  Map disk volumes.                                  ***
//*    Update:   2006/08/19                                         ***
//*                                                                 ***
//*********************************************************************
//*
//STEP1    EXEC PGM=DISKMAP,PARM='MAP',REGION=128K
//

