# Course content

## KeyNote

This course is made in a way to prepare you to the work context of CGI, we will see and work with technologies that CGI is currently using in most of thiers projects. The goal for this course is to learn how to use scripts languages to automate process. 

## Course planning

----
### WEEK 1 COBOL BASICS
----

### _Course 1_ - **Presentation of the course & table turn**

* -> Teacher background & objectives presentations
* -> Students background, objectives & expectations
* -> Couse planning
* -> Quick history of Cobol (why does it exists)

### _Course 2_ - **Basic of Mainframe**

* -> Compare cobol to other languages
* -> Setup workspace for Cobol (https://ibm.github.io/zopeneditor-about/Docs/getting_started.html#privacy-notice)
* -> View the basic of the language by putting togheter a Hello the world
* -> Explain the concepts of Cobol language

### _Course 3_ - **Concept of procedural programmation in Cobol**

* -> View the concept of script division
* -> View the script formatting in mainframe
* -> View the basic operators
* -> Make a few simple playground program to play around

### _Course 4_ - **Storage & function declaration**

* -> View the variable declaration in cobol
* -> View how to declare and call functions in cobol
* -> View how to use JCL with a small cobol program
* -> Assemble a small program with everything seen in the course

----
## WEEK 2 COBOL ADVANCED
----

### _Course 5_ - **Quick exam & view library loading**

* -> Quick test to validate knowledge
* -> Explore JCL functionalities
* -> Exercice of dataset sorting
* -> View library usage

### _Course 6_ - **Personal project in cobol**

* -> Define personal project & validate with the teacher
* -> Start working on the project
* -> Presentation of the project & Code review in group

### _Course 7_ - **Return on everything seen**

* -> Revision of everything seen during the first 2 weeks
* -> Kahoot in group (quizz in group)
* -> Prepare the subjects for the exam

### _Course 8_ - **Theoric exam on cobol**

* -> Answering any question before the test
* -> Paper exam

----
## WEEK 3 POWERSHELL
----

### _Course 9_ - **History of powershell & basic of powershell**

* -> Learn what is powershell and why it exists
* -> Why would you rather use powershell for certain projects
* -> Basic usage commands & life hacks
* -> Explorer powershell ISE
* -> Make a simple hello the world

### _Course 10_ - **Storage and condition**

* -> View condition syntax in powershell
* -> View variable declaration
* -> Create a small program with condition & variables
* -> Code review in group

### _Course 11_ - **Process handling & web requests**

* -> View how to use multiple scripts all togheter
* -> View log redirection & execution commands
* -> View the existings web requests tools in powershell
* -> Small project & code review in group

### _Course 12_ - **Revision & quick test**

* -> 
* -> 
* -> 
* ->

----
## WEEK 4 UNIX/SHELL
----

### _Course 13_ - **History of UNIX & basics**

* -> 
* -> 
* -> 
* ->

### _Course 14_ - **Permissions & Kernel layers presentation**

* -> 
* -> 
* -> 
* ->

### _Course 15_ - ****

* -> 
* -> 
* -> 
* ->

### _Course 16_ - ****

* -> 
* -> 
* -> 
* ->

----
## WEEK 5 VERSIONING & CI/CD
----

### _Course 17_ - ****

* -> 
* -> 
* -> 
* ->

### _Course 18_ - ****

* -> 
* -> 
* -> 
* ->

### _Course 19_ - ****

* -> 
* -> 
* -> 
* ->

----
## WEEK 6 REVISION
----

### _Course 20_ - ****

* -> 
* -> 
* -> 
* ->

### _Course 21_ - ****

* -> 
* -> 
* -> 
* ->

### _Course 22_ - ****

* -> 
* -> 
* -> 
* ->

----
## WEEK 7
----

### VACATION

----
## WEEK 8 FINAL PROJECT
----

### _Course 22 - 23 - 24_ - **Final project (Create a pipeline)**

* -> Create a powershell script

The powershell script must grab information on a website, parse the information to be in the right format. There will be some different interaction done to the information based on a few conditions. All files will be available on a static server.

* -> Create a shell script

The shell script will read a JSON configuration, use the information that the powershell script has generated, make it into an html report using a CLI tool provided by the teacher & the reports will be store on the static server.

* -> Create a cobol script

The cobol script must make a CLI display which the user will use to interact with the system that the student has put in place. The CLI must be able to trigger a report generation & will be a catalog of all the generated report.

* -> Assemble a Pipeline on TeamCity

This step will be done with the teacher with everyone to put in place the differents scripts made during the project on a teamcity server.

### _Bonus_ - **Install archlinux from scratch on VirtualBox** 

* -> Compose & assemble an archlinux os using shell commands

This laboratory will give a 10% bonus rate to the students that are able to achieve and understand how to install arch linux distribution. If students are able to install such a system, they are truly passionated, talented & they absolutly understand the foundation of scripting. They will outclass a lot of people in the understanding of shell and the UNIX core of linux if they can explain the step of installing archlinux.

## Source

Complete course -> https://riptutorial.com/fr/cobol


Command line instructions -> https://medium.com/@yvanscher/7-cobol-examples-with-explanations-ae1784b4d576


Formatting -> https://www.ibm.com/support/knowledgecenter/SSQ2R2_9.1.1/com.ibm.etools.rdz.language.editors.doc/topics/tzdebeformatter_cobol.html


Sample program -> http://www.simotime.com/nxcsys01.htm
