# Software requirements

* Ubuntu on windows
* Visual Studio Code
* Java 8 JRE
* OpenCobol CLI
* Virtualbox

# Configuration

* Add $JAVA_HOME to environment variables

**Make sure that the network doesn't block ubuntu on windows**

# Resource requirements

* Ubuntu VPS (virtual private server) -> 2Go RAM & 50Go SSD

How to connect to the VPS, is port 22 open?

# Hardware

* I use my laptop, I have a VGA & HDMI port, I need to be able to mirror my screen on the projector
