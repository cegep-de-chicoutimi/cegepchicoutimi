
# Projet - Client request analysis

Le projet permet d'amasser tous les logs de support au client via clavardage sur internet et d'en faire une base de connaissance afin de répondre automatiquement aux clients quand ils ont des questions plutôt simple. Ce projet permettera de sauver du temps pour notre division de service à la clientèle.


## Requirements

OpenCobol => 1.1.0


Nous avons choisi d'utiliser Cobol étant donné que nos programmeurs sont formé avec le language et que la majorité de nos systèmes sont déjà en Cobol, de plus nous avons des serveurs mainframes qui nous coute moins cher étant donné que nous nous en occupons nous même.

## Data

Nous avons reçu un exemple de quoi les données pouvaient avoir l'air grace à l'équipe de base de donnée. Les données de tests sont dans le fichier "datadump.txt"
