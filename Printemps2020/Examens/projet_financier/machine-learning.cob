       IDENTIFICATION DIVISION. 
       PROGRAM-ID. CLIENTHELPER.

       ENVIRONMENT DIVISION. 
          INPUT-OUTPUT SECTION.
          FILE-CONTROL.
            SELECT KEYWORD-STRUCTURE ASSIGN TO 'keyword.txt'
            ORGANIZATION IS SEQUENTIAL.
            
            SELECT SENTENSE-STRUCTURE ASSIGN TO 'datadump.txt'
            ORGANIZATION IS SEQUENTIAL.

       DATA DIVISION. 
          FILE SECTION. 
          FD KEYWORD-STRUCTURE.
          01 KEYWORD.
           02 KEYWORD-WORD PIC X(10).
           02 KEYWORD-REQUIREMENT PIC 9.
         
          FD SENTENSE-STRUCTURE.
          01 SENTENSE.
           02 SENTENSE-WORD occurs 10 times pic X(10).
           02 SENTENSE-REQUIREMENT pic 9.

          WORKING-STORAGE SECTION. 
          01 END-OF-FILE PIC Z(1).
          01 SENTENSE-WORD-POINTER PIC 99.
          
          01 USER_INPUT PIC X.

          01 BUFFER_WORD PIC X(10).
          01 BUFFER_SENTENSE PIC X(100).
          01 BUFFER_RANDOM PIC 9.

          01 REQUIREMENT_PREDICTION PIC X.
          01 COUNT_OF_REQUIRED PIC 99.
          01 COUNT_OF_NOT_REQUIRED PIC 99.

       PROCEDURE DIVISION.
           
           DISPLAY "Type 'o' if you want to skip the writing process".
           ACCEPT USER_INPUT.

           PERFORM WITH TEST BEFORE UNTIL USER_INPUT = "o" 
              PERFORM PROMPT_SENTENSE
               DISPLAY "(o) to continue"
               ACCEPT USER_INPUT
           END-PERFORM

           DISPLAY "q is for quit".

           PERFORM UNTIL USER_INPUT = "q"
               PERFORM READ_SENTENSE
               PERFORM PREDICT_REQUIREMENT
               PERFORM DISPLAY_SENTENSE 
               DISPLAY SPACE
               DISPLAY "require assistance (1) /" 
               " doesn't require assistance (2) /" 
               " correct (o)"
               ACCEPT USER_INPUT 
               PERFORM UPDATE_KEYWORD_DICTIONARY
           END-PERFORM.
       
           PERFORM END_PROGRAM.

       UPDATE_KEYWORD_DICTIONARY.
           MOVE 0 TO SENTENSE-WORD-POINTER.       
           
           IF USER_INPUT NOT = "o"
           PERFORM UNTIL SENTENSE-WORD-POINTER = 10
               ADD 1 TO SENTENSE-WORD-POINTER
               MOVE SENTENSE-WORD(SENTENSE-WORD-POINTER) TO KEYWORD-WORD 
               MOVE USER_INPUT TO KEYWORD-REQUIREMENT
               PERFORM WRITE_KEYWORD  
           END-PERFORM.

       PREDICT_REQUIREMENT.
           MOVE 0 TO SENTENSE-WORD-POINTER.       

           PERFORM UNTIL SENTENSE-WORD-POINTER = 10
               ADD 1 TO SENTENSE-WORD-POINTER
               MOVE SENTENSE-WORD(SENTENSE-WORD-POINTER) TO BUFFER_WORD 
               PERFORM CHECK_WORD_OCCURENCE
           END-PERFORM.

           IF COUNT_OF_NOT_REQUIRED < COUNT_OF_REQUIRED 
               DISPLAY "PREDICTION IS REQUIRE ASSISTANCE".

           IF COUNT_OF_REQUIRED < COUNT_OF_NOT_REQUIRED
               DISPLAY "PREDICTION IS DOES NOT REQUIRE ASSISTANCE".

           IF COUNT_OF_NOT_REQUIRED = COUNT_OF_REQUIRED 
               DISPLAY "PREDICTION IS AMBIGUOUS".
       
       CHECK_WORD_OCCURENCE.
           OPEN INPUT KEYWORD-STRUCTURE  

           MOVE 0 TO END-OF-FILE.
           MOVE 0 TO COUNT_OF_NOT_REQUIRED.
           MOVE 0 TO COUNT_OF_REQUIRED.

           PERFORM UNTIL END-OF-FILE = 1
              IF KEYWORD-WORD = BUFFER_WORD 
                  IF KEYWORD-REQUIREMENT = 1
                      ADD 1 TO COUNT_OF_REQUIRED
                  END-IF 

                  IF KEYWORD-REQUIREMENT = 2
                      ADD 1 TO COUNT_OF_NOT_REQUIRED 
                  END-IF 
              END-IF 
              READ KEYWORD-STRUCTURE   
                 AT END MOVE 1 TO END-OF-FILE
              END-READ
           END-PERFORM.

           CLOSE KEYWORD-STRUCTURE. 
       
       PROMPT_SENTENSE.
           DISPLAY "ENTER 10 WORD SENTENSE"
           MOVE 0 TO SENTENSE-WORD-POINTER
           PERFORM UNTIL SENTENSE-WORD-POINTER = 10
               ADD 1 TO SENTENSE-WORD-POINTER
               ACCEPT SENTENSE-WORD(SENTENSE-WORD-POINTER)
           END-PERFORM.
           DISPLAY "ENTER REQUIREMENT (1) REQ / (2) NO REQ".
           ACCEPT SENTENSE-REQUIREMENT.
           PERFORM WRITE_SENTENSE.

       DISPLAY_SENTENSE.
           MOVE 0 TO SENTENSE-WORD-POINTER.

           PERFORM UNTIL SENTENSE-WORD-POINTER = 10
              ADD 1 TO SENTENSE-WORD-POINTER
              DISPLAY SENTENSE-WORD(SENTENSE-WORD-POINTER)
           END-PERFORM.

       READ_SENTENSE.
           OPEN INPUT SENTENSE-STRUCTURE 
           MOVE 0 TO END-OF-FILE.
          
           PERFORM UNTIL END-OF-FILE = 1

      * Will pick a random line in the file

              COMPUTE BUFFER_RANDOM  = 
              1 + 6 * (FUNCTION RANDOM(FUNCTION CURRENT-DATE(15:2)))

              IF BUFFER_RANDOM = 1
                  MOVE 1 TO END-OF-FILE 
              END-IF 
              READ SENTENSE-STRUCTURE  
                 AT END MOVE 1 TO END-OF-FILE
              END-READ
           END-PERFORM.

           CLOSE SENTENSE-STRUCTURE.

       WRITE_KEYWORD.
           OPEN EXTEND KEYWORD-STRUCTURE.  
           WRITE KEYWORD FROM KEYWORD.
           CLOSE KEYWORD-STRUCTURE.

       WRITE_SENTENSE.
           OPEN EXTEND SENTENSE-STRUCTURE.
           WRITE SENTENSE FROM SENTENSE.
           CLOSE SENTENSE-STRUCTURE. 

       END_PROGRAM.
           DISPLAY "END".
           STOP RUN.