# Partie théorique

## Cobol

* Savoir les différentes divisions d'un script cobol et à quoi elles servent
* Qu'est-ce qui arrive si on oubli de mettre un stop run
* C'est quoi la différence entre un [programme, une division, une section, un paragraphe, une phrase, un ordre et un charactère](https://www.tutorialspoint.com/cobol/cobol_program_structure.htm)
* Être capable d'identifier les types de base et de les représenter

## Mainframe

* Faire la différence entre les différents programmes Mainframe, HASP, ASP, TSO
* À quoi servent les datasets

## JCL

* Être capable d'identifier les différentes parties d'une carte JCL

# Partie pratique

* Être capable de débugger des petits programme cobol qui ne font pas la bonne chose
